/**
 * @param {number} n
 * @return {boolean}
 */

function SquaresDigitNumbers(a) 
{ 
    sum = 0; 
    while (a!=0) 
    { 
        number = Math.trunc(a % 10); 
        sum += number * number; 
        a = Math.trunc(a / 10); 
    } 
    return sum; 
}

var isHappy = function(a) 
{
    var array = new Set(); 
    array.add(a);
    while (1) 
    { 
        if (a == 1) return true; 
        a = SquaresDigitNumbers(a); 
        if (array.has(a)) return false; 
        array.add(a);
    } 
    return false; 
};

rezult = isHappy(19);
console.log(rezult);