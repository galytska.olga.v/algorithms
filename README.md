# Laboratory work 
Laboratory work on the topic: "Algorithms"
***
Prepared by Halytska Olga
***
It is necessary to check if the number is lucky.  
The lucky number is the number that, as a result of the addition of its squared digits, turns into one.  I noticed that the number is not happy when one day, as a result of the manipulations described above, it turns into itself.  

First, I created a function that will calculate the sum of the square of the digits of a number.  Then, in the second function, I wrote all these numbers into a list until the number either turned into one, or turned into itself.
***
Algorithm complexity O(n^2)
***
<img src="https://gitlab.com/galytska.olga.v/algorithms/-/raw/feature/img/Success.jpg"></img>
***